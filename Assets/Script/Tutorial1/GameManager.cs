﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject gPlayer;
    public List<GameObject> gBoxs;

    private int index;
    private bool hopping;

    void Start () {
        index = 1;
        Application.targetFrameRate = 30;
    }

    void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!hopping)
                StartCoroutine(Jump(gBoxs[index].transform.position, 0.4f));
        }
#else
        if (Input.touchCount > 0)
        {
            StartCoroutine(Jump(gBoxs[index].transform.position, 0.5f));
        }
#endif
    }

    IEnumerator Jump(Vector3 target, float time)
    {
        target = new Vector3(target.x, 1f, target.z);

        hopping = true;

        var startPos = gPlayer.transform.position;
        var timer = 0.0f;

        while (timer <= 1.0f)
        {
            var height = Mathf.Sin(Mathf.PI * timer) * 3f;
            gPlayer.transform.position = Vector3.Lerp(startPos, target, timer) + Vector3.up * height;

            timer += Time.deltaTime / time;
            yield return null;
        }

        index++;
        if(index < gBoxs.Count)
            hopping = false;
    }
}
