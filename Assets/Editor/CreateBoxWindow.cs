﻿using UnityEditor;
using UnityEngine;

public class CreateBoxWindow : EditorWindow
{
    string strBoxContent = "";
    float minXRange = 0f;
    float maxXRange = 0f;

    float minYRange = 0f;
    float maxYRange = 0f;

    int boxNumber = 0;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Example/Create Boxs")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        CreateBoxWindow window = (CreateBoxWindow)EditorWindow.GetWindow(typeof(CreateBoxWindow));
        window.ShowUtility();
    }

    void OnGUI()
    {
        GUILayout.Label("Name of box content", EditorStyles.boldLabel);
        strBoxContent = EditorGUILayout.TextField("Object name content boxs", strBoxContent);

        GUILayout.Space(10f);
        GUILayout.Label("Create number of boxs", EditorStyles.boldLabel);
        boxNumber = EditorGUILayout.IntField("Number of boxs", boxNumber);

        GUILayout.Space(10f);
        GUILayout.Label("X Range", EditorStyles.boldLabel);
        minXRange = EditorGUILayout.FloatField("Min X Range", minXRange);
        maxXRange = EditorGUILayout.FloatField("Max X Range", maxXRange);

        GUILayout.Space(10f);
        GUILayout.Label("Y Range", EditorStyles.boldLabel);
        minYRange = EditorGUILayout.FloatField("Min Y Range", minYRange);
        maxYRange = EditorGUILayout.FloatField("Max Y Range", maxYRange);

        GUILayout.Space(10f);
        if (GUILayout.Button("Create"))
        {

        }

        if (GUILayout.Button("Close"))
        {
            this.Close();
        }
    }
}